# Start with a base Linux image
FROM alpine:latest

# Install required packages
RUN apk --no-cache add curl

# Set up Migrate installation
ENV MIGRATE_VERSION=v4.15.0
RUN curl -L https://github.com/golang-migrate/migrate/releases/download/${MIGRATE_VERSION}/migrate.linux-amd64.tar.gz | tar xvz && \
    mv migrate.linux-amd64 /usr/local/bin/migrate

# Set the working directory in the container
WORKDIR /app

# Copy your application code into the container
COPY . .

# Define the startup command
CMD ["./api"]
