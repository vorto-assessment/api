# API (Backend)

## Getting started

1. Run your local web server environment & create your own database

2. Migrate the sql schema with the command below by replacing {username}, {password}, {port}, {database_name} with your local machine credentials

```
migrate -path database/migration/ -database "mysql://{username}:{password}@tcp(localhost:{port})/{database_name}" -verbose up

```

## Initiate your api

Start your api by running the command below with the local url as http://localhost:8080

```
go run main.go

```
