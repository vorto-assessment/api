package main

import (
    "net/http"
	"github.com/gorilla/handlers"
    "github.com/gorilla/mux"
    "github.com/test/api/src"
    "github.com/test/api/config"
    "github.com/joho/godotenv"
)

func main() {
    godotenv.Load()
    // Initialize the database connection
    config.InitDB()

    // Set up the router and routes
    router := mux.NewRouter()
    api.SetRoutes(router)

    // Start the server
    http.Handle("/", router)
    // http.ListenAndServe(":8080", nil)
	http.ListenAndServe(":8080",
		handlers.CORS(
			handlers.AllowedOrigins([]string{"*"}),
			handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "OPTIONS"}),
			handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
		)(router))
}
