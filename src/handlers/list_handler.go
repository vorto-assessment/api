package handlers

import (
	"encoding/json"
	"net/http"
    "strconv"
    "github.com/gorilla/mux"
	"github.com/test/api/src/models"
	"github.com/test/api/config"
    "time"
    "log"
    "bytes"
    "fmt"
    "errors"
)
// ErrListNotFound is a custom error for list not found
var ErrListNotFound = errors.New("List not found")

// GetListHandler is the handler for getting a list
func GetListHandler(w http.ResponseWriter, r *http.Request) {

	// Fetch list from the database
	lists, err := getListFromDB()
	if err != nil {
		http.Error(w, "Failed to fetch list", http.StatusInternalServerError)
		return
	}

	// Convert list to JSON and send the response
	response, err := json.Marshal(lists)
	if err != nil {
		http.Error(w, "Failed to marshal JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

// getListFromDB retrieves all lists from the database
func getListFromDB() ([]models.List, error) {
	// Execute the SQL query to fetch all lists
	rows, err := config.DB.Query("SELECT * FROM list")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var lists []models.List

	for rows.Next() {
		var list models.List
        var created_at, updated_at string
		err := rows.Scan(&list.ListID, &list.Title, &created_at, &updated_at)
		if err != nil {
			return nil, err
		}
        // Parse the string representations of created_at and updated_at into time.Time
        list.CreatedAt, _ = parseTime(created_at)
        list.UpdatedAt, _ = parseTime(updated_at)
 
		lists = append(lists, list)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return lists, nil
}

func parseTime(value string) (time.Time, error) {
   // Set a default date and time format
   layout := "2006-01-02 15:04:05"

   // Parse the string representation of time using the default layout
   return time.Parse(layout, value)
}

// CreateListHandler is the handler for creating a list
func CreateListHandler(w http.ResponseWriter, r *http.Request) {
    // Parse request body to get user data
    var list models.List

    // Read the request body into a buffer
    var bodyBuffer bytes.Buffer
    _, err := bodyBuffer.ReadFrom(r.Body)
    if err != nil {
        http.Error(w, "Failed to read request body", http.StatusInternalServerError)
        return
    }

    // Decode the buffer into the list variable
    err = json.NewDecoder(&bodyBuffer).Decode(&list)
    if err != nil {
        http.Error(w, "Invalid JSON in request body", http.StatusBadRequest)
        return
    }

    // Call a function to create the user (you need to implement this)
    createdList, err := createListInDB(list)
    if err != nil {
        http.Error(w, "Failed to create list", http.StatusInternalServerError)
        return
    }

    // Convert the created user to JSON and send the response
    response, err := json.Marshal(createdList)
    if err != nil {
        log.Println("Error marshaling JSON:", err)
        http.Error(w, "Failed to marshal JSON", http.StatusInternalServerError)
        return
    }

    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusCreated)
    w.Write(response)
}

// createListInDB insert a new list into the database
func createListInDB(list models.List) (models.List, error) {
    // Prepare the SQL query
    query := "INSERT INTO list (title, created_at) VALUES (?, ?)"
    // log.Printf("SQL Query: %s\n", query)

    // Execute the query and get the result
    result, err := config.DB.Exec(query, list.Title, time.Now())
	
    if err != nil {
        return models.List{}, err
    }

    // Get the ID of the newly inserted user
    listID, err := result.LastInsertId()
    if err != nil {
        return models.List{}, err
    }

    // Fetch the complete list data from the database
    createdList := models.List{
        ListID:    listID,
        Title:     list.Title,
        CreatedAt: time.Now(),  // Set the creation time only once
    }

    return createdList, nil
}

// UpdateListHandler is the handler for updating a list
func UpdateListHandler(w http.ResponseWriter, r *http.Request) {
	// Extract the list ID from the URL parameters
	vars := mux.Vars(r)
	listIDStr, ok := vars["id"]
	if !ok {
		http.Error(w, "Missing list ID in URL", http.StatusBadRequest)
		return
	}

	// Parse the list ID string to an integer
	listID, err := strconv.ParseInt(listIDStr, 10, 64)
	if err != nil {
		http.Error(w, "Invalid list ID in URL", http.StatusBadRequest)
		return
	}

	// Parse request body to get updated list data
	var updatedList models.List
	err = json.NewDecoder(r.Body).Decode(&updatedList)
	if err != nil {
		log.Println("Error decoding JSON:", err)
		http.Error(w, "Invalid JSON in request body", http.StatusBadRequest)
		return
	}

	// Call a function to update the list (you need to implement this)
	updatedList, err = updateListInDB(listID, updatedList)
    if err != nil {
		// Check if the error is due to the list not found
		if err == ErrListNotFound {
			http.Error(w, fmt.Sprintf("List with ID %d not found", listID), http.StatusNotFound)
		} else {
			// Handle other errors
			log.Println("Error updating list in DB:", err)
			http.Error(w, "Failed to update list", http.StatusInternalServerError)
		}
		return
	}

	// Convert the updated list to JSON and send the response
	response, err := json.Marshal(updatedList)
	if err != nil {
		log.Println("Error marshaling JSON:", err)
		http.Error(w, "Failed to marshal JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

// updateListInDB update an existing list from the database
func updateListInDB(listID int64, updatedList models.List) (models.List, error) {
	// Prepare the SQL query for updating the list
	query := "UPDATE list SET title = ?, updated_at = ? WHERE list_id = ?"
	log.Printf("SQL Query: %s\n", query)

	// Execute the query and get the result
	result, err := config.DB.Exec(query, updatedList.Title, time.Now(), listID)
   
	if err != nil {
		log.Printf("Error executing SQL query: %s\n", err)
		return models.List{}, err
	}

	// Check if any rows were affected by the update
	rowsAffected, err := result.RowsAffected()
  
	if err != nil {
		log.Printf("Error getting rows affected: %s\n", err)
		return models.List{}, errors.New("List not found")
	}

	// If no rows were affected, the list with the given ID doesn't exist
	if rowsAffected == 0 {
        // log.Printf("List with ID %d not found\n", listID)
		return models.List{}, ErrListNotFound
	}

	// Fetch the updated list data from the database
	updatedList.ListID = listID
	updatedList.UpdatedAt = time.Now()

	return updatedList, nil
}

func DeleteListHandler(w http.ResponseWriter, r *http.Request) {

	// Extract the list ID from the URL parameters
	vars := mux.Vars(r)
	listIDStr, ok := vars["id"]
	if !ok {
		http.Error(w, "Missing list ID in URL", http.StatusBadRequest)
		return
	}

	// Parse the list ID string to an integer
	listID, err := strconv.ParseInt(listIDStr, 10, 64)
	if err != nil {
		http.Error(w, "Invalid list ID in URL", http.StatusBadRequest)
		return
	}

	// Call a function to delete the list (you need to implement this)
	err = deleteListFromDB(listID)
	if err != nil {
		// Check if the error is due to the list not found
		if err == ErrListNotFound {
			http.Error(w, fmt.Sprintf("List with ID %d not found", listID), http.StatusNotFound)
		} else {
			// Handle other errors
			log.Println("Error deleting list from DB:", err)
			http.Error(w, "Failed to delete list", http.StatusInternalServerError)
		}
		return
	}

	// Respond with a success message
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	response := map[string]string{"message": "List deleted successfully"}
	json.NewEncoder(w).Encode(response)
}

// deleteListFromDB deletes a list from the database by ID
func deleteListFromDB(listID int64) error {
	// Prepare the SQL query for deleting the list
	query := "DELETE FROM list WHERE list_id = ?"
	log.Printf("SQL Query: %s\n", query)

	// Execute the query and get the result
	result, err := config.DB.Exec(query, listID)
	if err != nil {
		log.Printf("Error executing SQL query: %s\n", err)
		return err
	}

	// Check if any rows were affected by the delete
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Printf("Error getting rows affected: %s\n", err)
		return err
	}

	// If no rows were affected, the list with the given ID doesn't exist
	if rowsAffected == 0 {
		return ErrListNotFound
	}

	return nil
}