package models

import "time"

// List represents a record in the 'list' table
type List struct {
    ListID    int64     `json:"list_id"`
    Title     string    `json:"title"`
    CreatedAt time.Time `json:"created_at"`
    UpdatedAt time.Time `json:"updated_at"`
}