package api

import (
    "github.com/gorilla/mux"
    "github.com/test/api/src/handlers"
)


// SetRoutes initializes and sets up the API routes
func SetRoutes(r *mux.Router) {
	// get list
    r.HandleFunc("/list", handlers.GetListHandler).Methods("GET")
    // create list
	r.HandleFunc("/list", handlers.CreateListHandler).Methods("POST")
	// // update list by id
    r.HandleFunc("/list/{id:[0-9]+}", handlers.UpdateListHandler).Methods("PUT")
    // // delete list by id
    r.HandleFunc("/list/{id:[0-9]+}", handlers.DeleteListHandler).Methods("DELETE")
}
